import os

from authentic2.settings import MIDDLEWARE

ALLOWED_HOSTS = ['localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'authentic2-auth-fedict',
    }
}

LANGUAGE_CODE = 'en'
A2_AUTH_SAML_ENABLE = False

MELLON_ADAPTER = ['authentic2_auth_fedict.adapters.AuthenticAdapter']
MELLON_LOGIN_URL = 'fedict-login'
MELLON_PUBLIC_KEYS = ['./tests/saml.crt']
MELLON_PRIVATE_KEY = './tests/saml.key'
MELLON_IDENTITY_PROVIDERS = [
    {
        'METADATA': open('./tests/metadata.xml').read(),
        'ENTITY_ID': 'https://idp.com/',
        'SLUG': 'idp',
    },
]

MELLON_ATTRIBUTE_MAPPING = {
    'last_name': '{attributes[surname][0]}',
    'first_name': '{attri,butes[givenName][0]}',
}

INSTALLED_APPS += ('authentic2_auth_fedict',)
AUTHENTICATION_BACKENDS += ('authentic2_auth_fedict.backends.FedictBackend',)

# test hook handlers
A2_HOOKS_PROPAGATE_EXCEPTIONS = True

A2_CACHE_ENABLED = False
