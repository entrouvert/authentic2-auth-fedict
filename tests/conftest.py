import django_webtest
import pytest

try:
    import pathlib
except ImportError:
    import pathlib2 as pathlib

from django.contrib.auth import get_user_model

User = get_user_model()
TEST_DIR = pathlib.Path(__file__).parent


@pytest.fixture
def app(request, db, settings, tmpdir):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    settings.MEDIA_DIR = str(tmpdir.mkdir('media'))
    return django_webtest.DjangoTestApp(extra_environ={'HTTP_HOST': 'localhost', 'wsgi.url_scheme': 'https'})


class AllHook:
    def __init__(self):
        self.calls = {}
        from authentic2 import hooks

        hooks.get_hooks.cache.clear()

    def __call__(self, hook_name, *args, **kwargs):
        calls = self.calls.setdefault(hook_name, [])
        calls.append({'args': args, 'kwargs': kwargs})

    def __getattr__(self, name):
        return self.calls.get(name, [])

    def clear(self):
        self.calls = {}


@pytest.fixture
def user(db):
    user = User.objects.create(
        username='john.doe',
        email='john.doe@example.net',
        first_name='John',
        last_name='Doe',
        email_verified=True,
    )
    user.set_password('john.doe')
    user.save()
    return user


@pytest.fixture
def hooks(settings):
    if hasattr(settings, 'A2_HOOKS'):
        hooks = settings.A2_HOOKS
    else:
        hooks = settings.A2_HOOKS = {}
    hook = hooks['__all__'] = AllHook()
    yield hook
    hook.clear()
    del settings.A2_HOOKS['__all__']


@pytest.fixture
def admin(db):
    user = User(username='admin', email='admin@example.net', is_superuser=True, is_staff=True)
    user.set_password('admin')
    user.save()
    return user


@pytest.fixture(autouse=True)
def clean_caches():
    from authentic2.apps.journal.models import event_type_cache

    event_type_cache.cache.clear()
