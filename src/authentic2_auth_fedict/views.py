# authentic2_auth_fedict - Fedict authentication for Authentic
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import random
import urllib.parse

from django.conf import settings
from django.contrib import messages
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, resolve_url
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt

try:
    import authentic2.utils.misc as a2_utils_misc
except ImportError:
    import authentic2.utils as a2_utils_misc

import mellon.utils
import mellon.views


class LoginView(mellon.views.LoginView):
    def authenticate(self, request, login, attributes):
        idp = mellon.utils.get_idp(attributes['issuer'])
        adapter = mellon.utils.get_adapters(idp)[0]
        user = adapter.lookup_user(idp, attributes)

        # extract nonce from next_url and record an additional authentication
        # event with it (as the event recorded in the adapter lacks the nonce).
        next_url = self.get_next_url(default=resolve_url(settings.LOGIN_REDIRECT_URL))
        try:
            nonce = urllib.parse.parse_qs(urllib.parse.urlparse(next_url).query)['nonce'][0]
        except (KeyError, IndexError):
            nonce = None
        a2_utils_misc.record_authentication_event(request, 'fedict', nonce=nonce)

        request_user = getattr(request, 'user', None) if request else None
        if request_user and request_user.is_authenticated and request_user.email:
            # get email from existing user
            user.email = request_user.email

        if not user.email:
            adapter.provision_attribute(user, idp, attributes)
            user.is_active = False
            user.deleted = None
            user.save()
            data = {}
            data['email'] = 'adresse@email.%s.invalid' % random.randint(0, 10000000)
            data['confirm_data'] = True
            data['valid_email'] = False
            data['skip_email_check'] = True
            data['user_id'] = user.id
            data['authentication_method'] = 'fedict'
            return HttpResponseRedirect(a2_utils_misc.build_activation_url(request, **data))
        user.is_active = True
        user.save()
        return super().authenticate(request, login, attributes)


login = transaction.non_atomic_requests(csrf_exempt(LoginView.as_view()))


def unlink(request):
    if not hasattr(request, 'user') or not hasattr(request.user, 'saml_identifiers'):
        return
    unlink_performed = False
    for saml_identifier in request.user.saml_identifiers.all():
        saml_identifier.delete()
        unlink_performed = True
    if unlink_performed:
        messages.success(request, message=_('Unlinking complete.'))
    return redirect('account_management')
