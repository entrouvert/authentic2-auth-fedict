$.fn.datetimepicker.dates['en'] = {
    days: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'],
    daysShort: ['dim', 'lun', 'mar', 'mer', 'jeu', 'ven', 'sam', 'dim'],
    daysMin: ['di', 'lu', 'ma', 'me', 'je', 've', 'sa', 'di'],
    months: ['janvier', 'f\xe9vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao\xfbt', 'septembre', 'octobre', 'novembre', 'd\xe9cembre'],
    monthsShort: ['Jan', 'F\xe9v', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sept', 'Oct', 'Nov', 'Déc'],
    meridiem: ['am', 'pm'],
    suffix: ['st', 'nd', 'rd', 'th'],
    today: 'Aujourd\'hui'
};
