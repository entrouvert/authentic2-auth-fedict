$(function() {
  $('input.date-widget').each(function(idx, elem) {
    var options = {
      format: "dd/mm/yyyy",
      autoclose: true,
      minView: 2,
      weekStart: 1,
      startView: 4,
    };
    if ($(elem).parent().prev().hasClass('errorlist')) {
      /* remove invalid value as it confuses the date picker */
      $(elem).val('');
    }
    if ($(elem).val()) {
      /* if there's already a valid value, set start view to month */
      options.startView = 2;
    }
    $(elem).datetimepicker(options).on('changeDate',
              function(ev) { $(this).data('datetimepicker').startViewMode = 2; });
  });

  $('[data-autocomplete-url]').each(function(idx, elem) {
    var autocomplete_url = $(elem).data('autocomplete-url');
    $(elem).autocomplete({
      source: function(request, response) {
        $.ajax({
          url: autocomplete_url,
          dataType: 'jsonp',
          data: {
            q: request.term
          },
          success: function( data ) {
            response( $.map(data.data, function(item) {
              return {label: item.text, value: item.label};
             }));
          }
        });
      },
      minLength: 2,
      open: function() {
        $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
      },
      close: function() {
        $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
      }
    });
  });
});
