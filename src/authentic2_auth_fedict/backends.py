# authentic2_auth_fedict - Fedict authentication for Authentic
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

import lasso
from authentic2.models import Attribute
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from mellon.backends import SAMLBackend
from mellon.utils import get_adapters, get_idp

logger = logging.getLogger(__name__)


class FedictBackend(SAMLBackend):
    def get_saml2_authn_context(self):
        # The Fedict will return one of:
        #  * urn:be:fedict:iam:fas:citizen:eid,
        #  * urn:be:fedict:iam:fas:citizen:token,
        #  * urn:be:fedict:iam:fas:enterprise:eid,
        #  * urn:be:fedict:iam:fas:citizen:token
        # but we do not expose this detail to the service provider as all it
        # needs to know is "strong authentication".
        return lasso.SAML2_AUTHN_CONTEXT_SMARTCARD_PKI

    def authenticate(self, request=None, **credentials):
        saml_attributes = credentials.get('saml_attributes') or {}
        if 'issuer' not in saml_attributes:
            logger.debug('no idp in saml_attributes')
            return None
        idp = get_idp(saml_attributes['issuer'])
        if not idp:
            logger.debug('unknown idp %s', saml_attributes['issuer'])
            return None
        adapters = get_adapters(idp, request=request)
        for adapter in adapters:
            if not hasattr(adapter, 'authorize'):
                continue
            if not adapter.authorize(idp, saml_attributes):
                return
        for adapter in adapters:
            if hasattr(adapter, 'lookup_by_attributes'):
                user = adapter.lookup_by_attributes(idp, saml_attributes)
        old_user = None
        for adapter in adapters:
            if not hasattr(adapter, 'lookup_user'):
                continue
            user = adapter.lookup_user(idp, saml_attributes)
            if user:
                request_user = getattr(request, 'user', None) if request else None
                if request_user != user and request_user.is_authenticated:
                    old_user = user
                    user = request_user
                    saml_identifier = old_user.saml_identifier
                    saml_identifier.user = user
                    saml_identifier.save(update_fields=['user'])
                    user.saml_identifier = saml_identifier
                    messages.success(request, _('Your account is now linked to your eID card.'))
                break
        else:  # no user found
            return
        for adapter in adapters:
            if not hasattr(adapter, 'provision'):
                continue
            # we attempt to provision the old user and then only copy the relevant attributes
            adapter.provision(old_user or user, idp, saml_attributes)
        # final identifier and attributes copy before deletion of old account
        if old_user and old_user != user:
            # copy all verified attributes to newly provisionned user
            if old_user.email_verified:
                user.email = old_user.email
                user.email_verified = True
            for attribute in Attribute.objects.all():
                value = getattr(old_user.verified_attributes, attribute.name, None)
                if value:
                    setattr(user.verified_attributes, attribute.name, value)
            logger.debug('deleting user %s, new fedict link manually created', old_user)
            old_user.delete()
        return user
