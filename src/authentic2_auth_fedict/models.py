# authentic2_auth_fedict - Fedict authentication for Authentic
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from authentic2.apps.authenticators.models import BaseAuthenticator
from authentic2.utils.misc import redirect_to_login
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _
from mellon.utils import get_idp, get_idps


class FedictAuthenticator(BaseAuthenticator):
    type = 'fedict'
    unique = True

    class Meta:
        verbose_name = _('Belgian eID')

    @property
    def manager_form_class(self):
        from .forms import FedictAuthenticatorForm

        return FedictAuthenticatorForm

    def login(self, request, *args, **kwargs):
        context = kwargs.get('context', {}).copy()
        submit_name = 'login-%s' % self.id
        if request.method == 'POST' and submit_name in request.POST:
            return redirect_to_login(request, login_url='fedict-login')
        context['submit_name'] = submit_name
        context.update(self.get_supported_methods())
        return render(request, 'authentic2_auth_fedict/login.html', context)

    def profile(self, request, *args, **kwargs):
        context = kwargs.get('context', {}).copy()
        user_saml_identifiers = request.user.saml_identifiers.all()
        for user_saml_identifier in user_saml_identifiers:
            user_saml_identifier.idp = get_idp(user_saml_identifier.issuer)
        context['user_saml_identifiers'] = user_saml_identifiers
        context.update(self.get_supported_methods())
        return render_to_string('authentic2_auth_fedict/profile.html', context, request=request)

    def get_supported_methods(self):
        try:
            idp = [x for x in list(get_idps()) if 'belgium.be' in x.get('ENTITY_ID')][0]
            authn_classref = idp['AUTHN_CLASSREF']
        except (IndexError, KeyError):
            authn_classref = ''
        return {
            'has_tokens': 'urn:be:fedict:iam:fas:citizen:token' in authn_classref
            or 'urn:be:fedict:iam:fas:citizen:Level300' in authn_classref,
            'has_itsme': 'urn:be:fedict:iam:fas:citizen:bmid' in authn_classref
            or 'urn:be:fedict:iam:fas:citizen:Level450' in authn_classref,
        }
