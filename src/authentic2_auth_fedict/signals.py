# authentic2_auth_fedict - Fedict authentication for Authentic
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from authentic2.models import Attribute, AttributeValue

from .adapters import AuthenticAdapter
from .models import FedictAuthenticator


def on_user_logged_in(sender, request, user, **kwargs):
    if not FedictAuthenticator.objects.filter(enabled=True).exists():
        return
    if user.backend == 'authentic2_auth_fedict.backends.FedictBackend':
        return
    attribute = None
    for attr_name in ('niss', 'nrn'):
        try:
            attribute = Attribute.objects.get(name=attr_name)
            break
        except Attribute.DoesNotExist:
            pass
    if not attribute:
        return
    try:
        av = AttributeValue.objects.with_owner(user).filter(attribute=attribute, verified=True)[0]
    except IndexError:
        return
    nrn = av.to_python()
    if not nrn:
        return
    adapter = AuthenticAdapter()
    adapter.provision_from_nrn(user, nrn)
