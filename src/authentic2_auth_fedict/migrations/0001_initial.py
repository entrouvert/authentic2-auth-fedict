# Generated by Django 2.2.26 on 2022-07-06 15:11

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('authenticators', '0003_auto_20220413_1504'),
    ]

    operations = [
        migrations.CreateModel(
            name='FedictAuthenticator',
            fields=[
                (
                    'baseauthenticator_ptr',
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to='authenticators.BaseAuthenticator',
                    ),
                ),
            ],
            options={
                'verbose_name': 'Belgian eID',
            },
            bases=('authenticators.baseauthenticator',),
        ),
    ]
