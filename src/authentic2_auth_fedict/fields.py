# authentic2_auth_fedict - Fedict authentication for Authentic
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import time
from urllib.parse import urljoin

import requests
from django import forms
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from gadjo.templatetags.gadjo import xstatic


class NrnField(forms.CharField):
    def validate(self, value):
        super().validate(value)
        if not value:
            return
        try:
            if (97 - int(value[:9]) % 97) != int(value[-2:]) and (97 - int('2' + value[:9]) % 97) != int(
                value[-2:]
            ):
                raise ValueError()
        except ValueError:
            raise forms.ValidationError(_('Invalid format for national registry number.'))


class DateWidget(forms.TextInput):
    class Media:
        css = {'all': ('authentic2_auth_fedict/css/datetimepicker.css',)}
        js = (
            xstatic('jquery', 'jquery.min.js'),
            'authentic2_auth_fedict/js/bootstrap-datetimepicker.js',
            'authentic2_auth_fedict/js/bootstrap-datetimepicker.fr.js',
            'authentic2_auth_fedict/js/support.js',
        )

    def __init__(self, attrs=None):
        if not attrs:
            attrs = {}
        attrs['class'] = 'date-widget'
        super(forms.TextInput, self).__init__(attrs=attrs)


class DateField(forms.CharField):
    widget = DateWidget

    def validate(self, value):
        super().validate(value)
        if not value:
            return
        for format_string in ('%d/%m/%Y', '%Y-%m-%d'):
            try:
                time.strptime(value, format_string)
            except ValueError:
                continue
            break
        else:
            raise forms.ValidationError(_('Invalid format'))


class StreetWidget(forms.TextInput):
    class Media:
        css = {'all': ('xstatic/themes/smoothness/jquery-ui.css',)}
        js = (
            xstatic('jquery', 'jquery.min.js'),
            'xstatic/jquery-ui.js',
            'authentic2_auth_fedict/js/support.js',
        )

    def __init__(self, attrs=None):
        if not attrs:
            attrs = {}
        attrs['class'] = 'street-widget'
        if getattr(settings, 'IMIO_STREETS_URL', None):
            street_url = settings.IMIO_STREETS_URL
        else:
            passerelle_url = list(settings.KNOWN_SERVICES['passerelle'].values())[0]['url']
            street_url = urljoin(passerelle_url, '/imio-liege-lisrue/lisrue-be/voies/')
        attrs['data-autocomplete-url'] = street_url
        super(forms.TextInput, self).__init__(attrs=attrs)


class StreetField(forms.CharField):
    widget = StreetWidget


class CountryWidget(forms.Select):
    class Media:
        css = {'all': ('xstatic/themes/smoothness/jquery-ui.css',)}
        js = (
            xstatic('jquery', 'jquery.min.js'),
            'xstatic/jquery-ui.js',
            'authentic2_auth_fedict/js/support.js',
        )

    def __init__(self, attrs=None):
        if not attrs:
            attrs = {}
        attrs['class'] = 'country-widget'
        if getattr(settings, 'IMIO_COUNTRY_URL', None):
            country_url = settings.IMIO_COUNTRY_URL
        else:
            passerelle_url = list(settings.KNOWN_SERVICES['passerelle'].values())[0]['url']
            country_url = urljoin(passerelle_url, '/csvdatasource/pays/data')
        try:
            self.choices = [
                (x['id'], x['text']) for x in requests.get(country_url, timeout=30).json()['data']
            ]
        except ValueError:
            self.choices = []
        super(forms.Select, self).__init__(attrs=attrs, choices=self.choices)


class CountryField(forms.CharField):
    widget = CountryWidget
    choices = []


class NumHouseField(forms.CharField):
    def validate(self, value):
        super().validate(value)
        if not value:
            return
        try:
            if not re.match('^[1-9][0-9]*$', value):
                raise ValueError()
        except ValueError:
            raise forms.ValidationError(getattr(settings, 'A2_NUMHOUSE_ERROR_MESSAGE', _('Invalid format')))


class NumPhoneField(forms.CharField):
    def validate(self, value):
        super().validate(value)
        if not value:
            return
        try:
            if not re.match('^(0|\\+|00)(\\d{8,})', value):
                raise ValueError()
        except ValueError:
            raise forms.ValidationError(getattr(settings, 'A2_NUMPHONE_ERROR_MESSAGE', _('Invalid format')))
