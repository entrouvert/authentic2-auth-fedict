# authentic2_auth_fedict - Fedict authentication for Authentic
# Copyright (C) 2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import include, path

from . import views

urlpatterns = [
    path('accounts/saml/', include('mellon.urls'), kwargs={'logout_next_url': '/logout/'}),
    path(
        'accounts/fedict/login/',
        views.login,
        name='fedict-login',
        kwargs={'template_base': 'authentic2/base.html'},
    ),
    path('accounts/fedict/unlink/', views.unlink, name='fedict-unlink'),
]
