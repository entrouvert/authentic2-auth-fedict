# authentic2_auth_fedict - Fedict authentication for Authentic
# Copyright (C) 2016-2022  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from authentic2_auth_saml.apps import AppConfig as SamlAppConfig
from django.contrib.auth.signals import user_logged_in
from django.utils.translation import gettext_lazy as _


class AppConfig(SamlAppConfig):
    name = 'authentic2_auth_fedict'
    default = True

    def ready(self):
        super().ready()

        from . import signals

        user_logged_in.connect(signals.on_user_logged_in)

    def a2_hook_event(self, name, **kwargs):
        if name == 'registration':
            if kwargs.get('authentication_method') == 'fedict':
                user = kwargs.get('user')
                user.backend = 'authentic2_auth_fedict.backends.FedictBackend'

    def get_a2_plugin(self):
        return Plugin()


class Plugin:
    def get_before_urls(self):
        from . import urls

        return urls.urlpatterns

    def registration_form_prefill(self, request):
        if request.token.get('first_name'):
            return [
                {
                    'first_name': [request.token.get('first_name')],
                    'last_name': [request.token.get('last_name')],
                }
            ]
        else:
            return [{'first_name': [], 'last_name': []}]

    def attribute_kinds(self):
        from . import fields

        def attribute_json_loads(x):
            if not x:
                return x
            try:
                return json.loads(x)
            except json.JSONDecodeError:
                # "compatibility" with native date/phone kinds
                return x

        return [
            {
                'label': _('National Register Number'),
                'name': 'nrn',
                'serialize': json.dumps,
                'deserialize': attribute_json_loads,
                'field_class': fields.NrnField,
            },
            {
                'label': _('Date'),
                'serialize': json.dumps,
                'deserialize': attribute_json_loads,
                'name': 'date',
                'field_class': fields.DateField,
            },
            {
                'label': _('Date'),
                'serialize': json.dumps,
                'deserialize': attribute_json_loads,
                'name': 'fedict_date',
                'field_class': fields.DateField,
            },
            {
                'label': _('Street'),
                'serialize': json.dumps,
                'deserialize': attribute_json_loads,
                'name': 'street',
                'field_class': fields.StreetField,
            },
            {
                'label': _('House number'),
                'serialize': json.dumps,
                'deserialize': attribute_json_loads,
                'name': 'num_house',
                'field_class': fields.NumHouseField,
            },
            {
                'label': _('Phone number'),
                'serialize': json.dumps,
                'deserialize': attribute_json_loads,
                'name': 'phone',
                'field_class': fields.NumPhoneField,
            },
            {
                'label': _('Phone number'),
                'serialize': json.dumps,
                'deserialize': attribute_json_loads,
                'name': 'fedict_phone',
                'field_class': fields.NumPhoneField,
            },
            {
                'label': _('Country'),
                'serialize': json.dumps,
                'deserialize': attribute_json_loads,
                'name': 'country',
                'field_class': fields.CountryField,
            },
        ]
