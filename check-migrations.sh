#!/bin/bash

set -e


# https://stackoverflow.com/questions/49778988/makemigrations-in-dev-machine-without-database-instance
CHECK_MIGRATIONS_SETTINGS=`mktemp`
trap "rm -f ${CHECK_MIGRATIONS_SETTINGS}" EXIT
cat <<EOF >${CHECK_MIGRATIONS_SETTINGS}
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.dummy',
    }
}
INSTALLED_APPS += ('authentic2_auth_fedict',)
EOF
TEMPFILE=`mktemp`
trap "rm -f ${TEMPFILE} ${CHECK_MIGRATIONS_SETTINGS}" EXIT

DJANGO_SETTINGS_MODULE=authentic2.settings AUTHENTIC2_SETTINGS_FILE=${CHECK_MIGRATIONS_SETTINGS}  django-admin makemigrations --dry-run --noinput authentic2_auth_fedict >${TEMPFILE} 2>&1 || true

if ! grep 'No changes detected' -q ${TEMPFILE}; then
   echo '!!! Missing migration detected !!!'
   cat ${TEMPFILE}
   exit 1
else
   exit 0
fi
